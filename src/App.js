import React from 'react';


import MainNavigation from './shared/components/Navigation/MainNavigation.js';
import Routes from './Routes'; 
import './App.css';

function App() {
  return (
    
      <div>
      <MainNavigation/> 
      <Routes/> 
      </div>
    
  );
}

export default App;
